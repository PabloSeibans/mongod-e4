db.getCollection("dueno").insertMany([
                                                {
                                                    "Nombres":"Pablo Fernandez",
                                                    "Edad": 22,
                                                    "nMascota":"Rocky"
                                                },
                                                {
                                                    "Nombres":"Erlinda Martinez",
                                                    "Edad":20,
                                                    "nMascota":"Princesa"
                                                },
                                                {
                                                    "Nombres":"Ramiro Valdez",
                                                    "Edad":19,
                                                    "nMascota":"Marmol"
                                                },
                                                {
                                                    "Nombres":"Gabriel Rojas",
                                                    "Edad": 23,
                                                    "nMascota":"Max"
                                                },
                                                {
                                                    "Nombres":"Luis Mancilla",
                                                    "Edad":25,
                                                    "nMascota":"Peluchin"
                                                }
                                            ])
                                                
var idPablo = ObjectId("62b2bbba724ef78f7862227a")
var idErlinda = ObjectId("62b2bbba724ef78f7862227b")
var idRamiro = ObjectId("62b2bbba724ef78f7862227c")
var idGabriel = ObjectId("62b2bbba724ef78f7862227d")
var idLuis = ObjectId("62b2bbba724ef78f7862227e")
                                                
db.getCollection("mascota").insertMany([
                                                {
                                                    "Nombres":"Rocky",
                                                    "Edad": 8,
                                                    "nDueno":"Pablo Fernandez",
                                                    "idDueno": idPablo
                                                },
                                                {
                                                    "Nombres":"Princesa",
                                                    "Edad":5,
                                                    "nDueno":"Erlinda Martinez",
                                                    "idDueno": idErlinda
                                                },
                                                {
                                                    "Nombres": "Marmol",
                                                    "Edad": 6,
                                                    "nDueno":"Ramiro Valdez",
                                                    "idDueno": idRamiro
                                                },
                                                {
                                                    "Nombres":"Max",
                                                    "Edad": 3,
                                                    "nDueno":"Gabriel Rojas",
                                                    "idDueno": idGabriel
                                                },
                                                {
                                                    "Nombres":"Peluchin",
                                                    "Edad":5,
                                                    "nDueno":"Luis Mancilla",
                                                    "idDueno": idLuis
                                                }
                                            ])
                                                
                                                
                                         
db.getCollection("dueno").createIndex({"Nombres":1})
db.getCollection("dueno").createIndex({"nMascota":1})


db.getCollection("mascota").createIndex({"Nombres":1})
db.getCollection("mascota").createIndex({"nDueno":1})

db.getCollection("dueno").find({})
db.getCollection("mascota").find({})

db.getCollection("mascota").find({"idDueno" : ObjectId("62b2bbba724ef78f7862227a")})

db.getCollection("mascota").find({"Edad" : {$gt: 4}})

db.getCollection("dueno").find({"Nombres": "Erlinda Martinez"})